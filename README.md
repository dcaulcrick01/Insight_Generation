

# Create 2 buckets:
 - aqua-datasetset
 - aqua-codes

# In aqua-dataset bucket, create a folder called 'raw'

# Navigate into shipment_load_trigger. You will find a zip file named shipment_load_trigger.zip. Upload the Zip into aqua-codes bucket you created

# Deploy your cloudformations
  - In the cloudformation folder deploy each of the cloudformation script in the order below
  * rds.yaml will create a rds instance with the lowest config
  * lambdaFunction.yaml will create a lambda function (pointing to the zipfile uploaded to aqua-codes ealier). Also create a role which will be attached to the lambda

# Once rds has been created, connect to the DB and create a table with the schema of our csv. Like below
    create table shippingdata
                    (
                    SKU varchar,
                    Quantity varchar,
                    Ship_Date varchar,
                    Cost varchar,
                    Cost_per_unit varchar
                    )

                    
# Create a Trigger Rule
  Once lambda has been created successfully create a trigger rule from the console.
  Navigate to the function created (shipment_load_trigger) on the console, collaspe function overview section then click add trigger and follow the instruction
   - choose s3
   - choose the aqua-raw-dataset(created earlier) as your bucket named
   - select 'put' as your event type
   - let the prefixes be 'raw\'
   - let suffixes be '.csv'
   - then click add



# To test test the lambda, drop ShippingData.csv into the aqua-datasetset bucket under 'raw' folder then go to the database table created to see the file content ingested