import psycopg2
import pandas as pd
import boto3
from io import BytesIO, StringIO
import os


csv_buffer = StringIO()
s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')
host=os.environ['host']
database=os.environ['database']
user=os.environ['user']
password=os.environ['password']
table_name='shipment'
port=5432
# conn_str= 'postgresql://my_dev:gabe_dev@database123.cn6ot0ieh9gk.us-east-1.rds.amazonaws.com:5432/aqua_db'
#os.environ['conn_str']
def create_db_conn():
    try:
        connection = psycopg2.connect(user=user,
                                    password=password,
                                    host=host,
                                    port=port,
                                    database=database)
    except Exception as e:
        print(e)
        print('Cannot Connect to DB')
    print("COnnected to DB")
    return connection
    
create_db_conn()

def lambda_handler(event, context):
    s3= event['Records'][0]['s3']
    bucket_name = s3['bucket']['name']
    file_name = s3['object']['key']
    obj = s3_client.get_object(Bucket=bucket_name, Key = file_name)
    context=''
    def clean_up(obj):
        df = pd.read_csv(obj['Body'])
        print('Dataframe Size is ', len(df))

        #Rename the columns
        #df.rename({'FNSKU0':'SKU','Qty Shipped (SellerCentral)':'Quantity', 'Shipment ETD':'Ship_Date', 'Shipping PI Amount (RMB) Source 1':'Cost'}, inplace=True)
        df.columns=['SKU','Quantity','Ship_Date','Cost']
        def remove_comma(strg):
            clean_str=strg.replace(',','')
            return clean_str

        #drop all invalid and incomplete rows 
        df.dropna(inplace=True)
        df['Cost']=df['Cost'].astype(str)
        df['Cost'] = df['Cost'].apply(remove_comma)
        df.drop(df.loc[df['Cost']=='-'].index, inplace=True)
        df.drop(df.loc[df['Quantity']=='-'].index, inplace=True)

        #covert Cost and Quantity to Float 
        df['Cost']=df['Cost'].astype(float)
        df=df[df.Quantity.apply(lambda x: x.isnumeric())]
        df["Quantity"] = pd.to_numeric(df["Quantity"], downcast="float")

        #compute the KPI “Cost_per_unit” (i.e. Cost/Quantity) as a new column.
        df['Cost_per_unit']=df['Cost']/df['Quantity']
        print('Dataframe Size is ', len(df))

        #write dataframe to temporary directory as csv
        temp_dir='/tmp/ShippingDataClean.csv'
        df.to_csv(temp_dir, index=False, header=False)

        #load the DF into rds
        insertquery = "select * from mobile"
        f = open(temp_dir, 'r')
        connection = create_db_conn()
        cursor=connection.cursor()
        try:
            cursor.copy_from(f, table_name, sep=",")
            connection.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            os.remove(temp_dir)
            print("Error: %s" % error)
            connection.rollback()
            cursor.close()
            return 1
        print("copy_from_file() done")
        cursor.close()
        f.close()
        os.remove(temp_dir)

    clean_up(obj)
# context=''
# lambda_handler(event, context)